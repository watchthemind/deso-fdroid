Deso is a social media platform.

Deso has 100% open source code. And, the social media data in Deso is stored in a public P2P database which any developer can access. Client side end to end encryption is used.

Deso aims to dethrone Meta(Facebook) and Google.
