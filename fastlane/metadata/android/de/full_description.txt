Deso ist eine Social-Media-Plattform.

Deso hat 100 % Open-Source-Code. Und die Social-Media-Daten in Deso werden in einer öffentlichen P2P-Datenbank gespeichert, auf die jeder Entwickler zugreifen kann. Es wird eine clientseitige Ende-zu-Ende-Verschlüsselung verwendet.

Deso zielt darauf ab, Meta (Facebook) und Google zu entthronen.
